#!/bin/bash

# example for using the script library pipe2amq_lib

source ./pipe2amq_lib

CFG='./test.properties'


RESULT=''
for (( blocksize=1 ; blocksize<15 ; blocksize++)) ; do
    start_when_needed "${CFG}" '../pipe2amq'
    RETRY=4
    BEFORE=$EPOCHSECONDS
    for (( i=0 ; i<2000 ; i=i+1 )) ; do
        send_burst_message "${CFG}" ${blocksize} "hello world, $i"
    done
    while ! send_burst_message "${CFG}" 0 && [ ${RETRY} -gt 0 ] ; do
        ((RETRY--))
    done
    AFTER=$EPOCHSECONDS
    DURATION="$((AFTER-BEFORE))"
    S=$((i/DURATION))
    RESULT="${RESULT}  ${blocksize}blocks:${DURATION}s:${S}msg/s"
    echo $RESULT
    sleep 16
done

for l in ${RESULT} ;
do
    echo $l
done

shutdown "${CFG}"
