#!/bin/bash

# example for using the script library pipe2amq_lib

source ./pipe2amq_lib

CFG='./test.properties'

start_when_needed "${CFG}" '../pipe2amq'

N=$EPOCHSECONDS
for (( i=0 ; i<50 ; i++ )) ; do
    send_message "${CFG}"  "hello world, $i"
done
M=$EPOCHSECONDS

echo "$((M-N)) seconds for $i messages"

shutdown "${CFG}"
