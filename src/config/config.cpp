
#include "config.hpp"

namespace config {
  config::config(const std::string &filename) {
    pt::read_ini(filename, tree);
    debug=tree.get<bool>("debug",false);
    if (debug) {
      std::cout << "config file " << filename << std::endl;
    }
  }


  bool config::has_key(const std::string &key) const {
    return tree.find(key)==tree.not_found();
  }
  

  bool config::get_debug() const {
    return debug;
  }
  
  std::string config::get_requestfifo() const {
    const auto res{tree.get<std::string>("requestfifo")};
    if (debug)
       std::cout << "get_requestfifo() -> " << res << std::endl;
    return res;
  }


  std::string config::get_fifoprefix() const {
    const auto res{tree.get<std::string>("fifoprefix")};
    if (debug)
      std::cout << "get_fifoprefix() -> " << res << std::endl;
    return res;
  }

  std::string config::get_Username() const {
    const auto res{tree.get<std::string>("Username")};
    if (debug)
      std::cout << "get_Username() -> " << res << std::endl;
    return res;
  }


  std::string config::get_Password() const {
    const auto res{tree.get<std::string>("Password")};
    //     if (debug)
    //        std::cout << "get_Password() -> " << res << std::endl;
    return res;
  }

  std::string config::get_Hostname() const {
    const auto res{tree.get<std::string>("Hostname")};
    if (debug)
      std::cout << "get_Hostname() -> " << res << std::endl;
    return res;
  }

  int config::get_port() const {
    const auto res{tree.get<int>("port", 5672)};
    if (debug)
      std::cout << "get_port() -> " << res << std::endl;
    return res;
  }
  
  
  std::string config::get_Virtualhost() const {
    const auto res{tree.get<std::string>("Virtualhost", "/")};
    if (debug)
      std::cout << "get_Virtualhost() -> " << res << std::endl;
    return res;
  }

  
  std::string config::get_routingKey() const {
    const auto res{tree.get<std::string>("routingKey")};
     if (debug)
       std::cout << "get_routingKey() -> " << res << std::endl;
    return res;
  }

  std::string config::get_exchange() const {
    const auto res{tree.get<std::string>("exchange")};
     if (debug)
      std::cout << "get_exchange() -> " << res << std::endl;
    return res;
  }

  std::string config::get_clientInstance() const {
    const auto res{tree.get<std::string>("clientInstance", "pipeAdaptor")};
    if (debug)
      std::cout << "get_clientInstance() -> " << res << std::endl;
    return res;
  }

  std::string config::get_createQueue() const {
    const auto res{tree.get<std::string>("createQueue", "")};
    if (debug)
      std::cout << "get_createQueue() -> " << res << std::endl;
    return res;
  }
  
  int config::get_maxRetry() const {
    const auto res{tree.get<int>("maxRetry", 2)};
    if (debug)
      std::cout << "get_maxRetry() -> " << res << std::endl;
    return res;
  }

  
  bool config::get_noAMQ() const {
    return tree.get<bool>("noAMQ", false);
  }

}
