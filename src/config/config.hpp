#ifndef HEADER_config
#define HEADER_config

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>
#include <string>
#include <set>
#include <exception>
#include <iostream>
#include <string>
namespace pt = boost::property_tree;

namespace config {
  class config {
  private:
    pt::ptree tree;
    bool has_key(const std::string &key) const;
    bool debug;
  public:
    config(const std::string &filename);
    bool        get_debug()          const;
    std::string get_requestfifo()           const;
    std::string get_fifoprefix()     const;
    std::string get_Username()       const;
    std::string get_Password()       const;
    std::string get_Hostname()       const;
    int         get_port()           const;
    std::string get_Virtualhost()    const;
    std::string get_exchange()       const;
    std::string get_routingKey()     const;
    std::string get_clientInstance() const;
    std::string get_createQueue()    const;
    int         get_maxRetry()       const;
    bool        get_noAMQ()          const;
  };


}

#endif
