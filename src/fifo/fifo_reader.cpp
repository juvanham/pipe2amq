#include "fifo_reader.hpp"
#include <string_view>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef DEBUG
#include<iostream>
#endif

namespace fifo {
  namespace fs=std::filesystem;

  fifo_reader::fifo_reader(std::string_view pth, bool dbg):
    path{pth},
    fd{},
    debug{dbg}
  {
    mkfifo(path.c_str(), 0620);
  }


  fifo_reader::~fifo_reader() {
    remove();
    close();
    buffer.clear();
  }


  bool fifo_reader::is_fifo() const {
    if (fs::exists(path))
      return fs::is_fifo(path);
    return false;
  }


  bool fifo_reader::remove() {
    if (!fs::exists(path))
      return false;
    if (!fs::is_fifo(path))
      return false;
    fs::remove(path);
    return true;
  }


  bool fifo_reader::open() {
    if (!is_fifo()) {
      close();
      return false;
    }
    if (fd)
      return true;
    int fd_=::open(path.c_str(), O_CLOEXEC | O_NOATIME |O_NONBLOCK | O_RDONLY);
    if (fd_) {
      fd=fd_;
      if (fcntl(*fd, F_GETFD)!=-1)
	return true;
    }
    fd.reset();
    return false;
  }


  bool fifo_reader::close() {
    if (!fd)
      return false;
    if (fcntl(*fd, F_GETFD)!=-1)
      ::close(*fd);
    if (debug)
      std::cout << "fifo_reader::close" << std::endl;
    fd.reset();
    return true;
  }

  bool fifo_reader::poll() {
    if (!open())
      return false;
    const auto read_chars {
      read(*fd, readbuffer.data(), readbuffer.size())
    };
    if (read_chars==-1) {
      return false;
    }
    if (read_chars==0)
      return false;
     if (debug)
       std::cout << "fifo_reader::poll ->" << read_chars << std::endl;    
    buffer.append(cbegin(readbuffer), cbegin(readbuffer)+read_chars);
    return true;
  }


  std::optional<std::string> fifo_reader::string() {
    const auto found=buffer.find('\x04');
    if (found==std::string::npos)
      return {};
    auto result{std::string(cbegin(buffer),cbegin(buffer)+found)};
    buffer.erase(0,found+1);
    while (buffer[0]=='\x0a' || buffer[0]=='\x0d' )
      buffer.erase(0,1);
    if (0 && debug) {
      int i{0};
      std::cout << "fifo_reader::string()" << std::endl;
      for (const auto s:result) {
	const int a{s};
	std::cout << (i++) << "   '" << s << "'  " << static_cast<int>(s)<<  "," << found << std::endl;
      }
    }
    return result;
  }


}
