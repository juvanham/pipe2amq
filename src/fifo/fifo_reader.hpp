#ifndef HEADER_fifo_reader
#define HEADER_fifo_reader

#include <filesystem>
#include <string>
#include <fstream>
#include <memory>
#include <optional>

namespace fifo {
  class fifo_reader {
    const std::filesystem::path path;
    const bool debug;   
    std::string buffer;
    std::optional<int> fd;
    std::array<char,64> readbuffer;
    bool open();
  public:
    fifo_reader(std::string_view  pth, bool dbg);
    virtual ~fifo_reader();
    bool is_fifo() const;
    bool close();
    bool remove();
    bool poll();
    std::optional<std::string> string(); 
  };
 
}


#endif
