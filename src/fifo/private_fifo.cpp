#include "private_fifo.hpp"
#include "fifo_reader.hpp"
#include <iostream>

namespace fifo {
  
  
  private_fifo::private_fifo(const config::config &cfg_,
			     sender::sender *const sender_,
			     std::string_view path) :
    cfg{cfg_},
    sender{sender_},
    fifo{std::make_unique<fifo_reader>(path, cfg.get_debug())}
  {
     if (cfg.get_debug()) {
       std::cout << "private_fifo(" << path << ")" << std::endl;
    }
  }

  private_fifo::~private_fifo() {
    if (fifo) {
      fifo->remove();
    }
    fifo.reset();
    
  }

  bool private_fifo::shutdown(bool success) {
    if (shutdown_state)
      return false;
    shutdown_state=true;
    if (success)
      fifo->remove();
    fifo->close();
    return true;
  }
  
  bool private_fifo::is_shutdown() {
    if (fifo && !fifo->is_fifo())
      shutdown_state=true;
    return shutdown_state;
  }
  
  bool private_fifo::handle_request() {
    const auto msg{fifo->string()};
    if (!msg) 
      return false;


    if (cfg.get_debug())
      std::cout << "ready to send: " << msg->c_str() << std::endl;
    bool success{
		 sender->send(*msg)
    };
    shutdown(success);
    
    return true;
  }
  
  std::pair<bool,bool> private_fifo::poll() {
    bool received{false};
    bool handled{false};
    if (!fifo) 
      return {received,handled};
    if (fifo->poll()) {
      handled|=handle_request();
      received|=true;
    }
    return {received,handled};
  }

}
