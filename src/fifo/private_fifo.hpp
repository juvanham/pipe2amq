#ifndef HEADER_private_fifo
#define HEADER_private_fifo

#include <string_view>
#include <memory>
#include "../config/config.hpp"
#include "../sender/sender.hpp"

namespace fifo {
  class fifo_reader;
  class private_fifo {
    const config::config &cfg;
    sender::sender *const sender;
    std::unique_ptr<fifo_reader> fifo;
    bool shutdown_state{false};
    bool handle_request();
    bool shutdown(bool success);
  public:
    private_fifo(const config::config &cfg_,
		 sender::sender *const sender_,
		 std::string_view path);
    ~private_fifo();
    std::pair<bool,bool> poll();
    bool is_shutdown();
  };
}

#endif
