#include "request_fifo.hpp"
#include "private_fifo.hpp"
#include "fifo_reader.hpp"
#include <iostream>
#include <algorithm>
#include <string>

namespace chrono=std::chrono;
namespace fifo {


  const std::string cmd_shutdown{"shutdown"};
  const std::string cmd_fifo{"fifo:"};
  const std::string cmd_timeout{"timeout"};


  request_fifo::request_fifo(const config::config &cfg_,
			     sender::sender *sender_
			     ):
    cfg{cfg_},
    sender{sender_},
    fifo{std::make_unique<fifo_reader>(cfg.get_requestfifo(),
				       cfg.get_debug()
				       )}
  {}


  request_fifo::~request_fifo() {
    private_fifos.clear();
  }


  bool request_fifo::create_private_fifo(std::string_view pth) {
    if (cfg.get_debug()) {
      std::cout << "create_private_fifo(" << pth << ")" << std::endl;
    }
    if (pth.find("/../")!=std::string::npos) {
      if (cfg.get_debug()) {
	std::cout << pth << " contains '..'" << std::endl;
      }
      return false;
    }
    if (!pth.starts_with(cfg.get_fifoprefix())) {
      if (cfg.get_debug()) {
	std::cout << pth << " does not match '" << cfg.get_fifoprefix() << std::endl;
      }
      return false;
    }
    private_fifos.push_back(std::make_unique<private_fifo>(cfg, sender, pth));
    if (cfg.get_debug()) {
      std::cout << "create_private_fifo(" << pth << ") done" << std::endl;
    }
    return true;
  }


  bool request_fifo::handle_request() {
    const auto msg{fifo->string()};
    if (!msg)
      return false;
    if (cfg.get_debug()) {
      std::cout << "request_fifo::handle_request(" << *msg << ")" << std::endl;
    }
    if (msg->compare(cmd_shutdown)==0) {
      if (fifo) {
        fifo->close();
        fifo.reset();
      }
      shutdown_state=true;
      return true;
    }
    if (msg->starts_with(cmd_fifo)) {
      std::string_view arg(msg->c_str(),msg->length());
      arg.remove_prefix(cmd_fifo.length());
      create_private_fifo(arg);
      return true;
    }
    if (msg->starts_with(cmd_timeout)) {
      const std::string arg{std::cbegin(*msg)+1+cmd_timeout.length(),std::cend(*msg)};
      if (auto val=std::stoul(arg);val>0)
        timeout_s=val;
      else
        timeout_s.reset();
      if (cfg.get_debug()) {
	std::cout << "timeout: " << ((timeout_s)?*timeout_s:0) << std::endl;
      }
      return true;
    }

    return false;
  }


  std::pair<bool,bool> request_fifo::poll() {
    bool received{false};
    bool handled{false};
    if (!fifo)
      return {received,handled};
    if (!fifo->is_fifo()) {
        shutdown_state=true;
	return {received,handled};
    }
    if (fifo->poll()) {
      handled|=handle_request();
      received|=true;
    }

    for (auto &private_fifo:private_fifos) {
      if (!private_fifo)
        continue;
      if (private_fifo->is_shutdown()) {
        private_fifo.reset();
        continue;
      }
      const auto [phandled,preceived]=private_fifo->poll();
      handled|=phandled;
      received|=preceived;
    }
    if (handled) {
      private_fifos.erase(
                          std::remove_if(begin(private_fifos),
                                         end(private_fifos),
                                         [](const auto &elm){
                                           return !elm;
                                         }),
                          end(private_fifos)
                          );
    }
    if (check_timeout(received)) {
      shutdown_state=true;
    }
    return {received,handled};
  }


  bool request_fifo::check_timeout(bool received) {
    if (!this->timeout_s)
      return false;
    if (received) {
      this->latest=chrono::steady_clock::now();
      return false;
    }
    const auto threshold{*timeout_s};
    const auto idleDuration{(chrono::steady_clock::now()-latest)};
    const auto idleSec{chrono::duration_cast<chrono::seconds>(idleDuration).count()};
    if (idleSec<threshold)
      return false;
    return true;
  }


}
