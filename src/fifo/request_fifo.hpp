#ifndef HEADER_request_fifo
#define HEADER_request_fifo

#include <vector>
#include <string_view>
#include <memory>
#include <chrono>
#include <optional>
#include "../config/config.hpp"
#include "../sender/sender.hpp"

namespace fifo {
  class fifo_reader;
  class private_fifo;

  class request_fifo {
    const config::config &cfg;
    sender::sender *const sender;
    std::unique_ptr<fifo_reader> fifo;
    std::vector<std::unique_ptr<private_fifo> > private_fifos;
    std::optional<unsigned long> timeout_s;
    std::chrono::time_point<std::chrono::steady_clock> latest;
    bool create_private_fifo(std::string_view pth);
    bool shutdown_state{false};
    bool check_timeout(bool received);
    bool handle_request();
  public:
    request_fifo(const config::config &cfg_, sender::sender * sender_);
    ~request_fifo();
    std::pair<bool,bool> poll();
    bool shutdown() const { return shutdown_state;}
  };


}

#endif
