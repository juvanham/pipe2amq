
#include <time.h>
#include <iostream>
#include "fifo/request_fifo.hpp"
#include "config/config.hpp"
#include "sender/wire_sender.hpp"

void default_config() {
  std::cout
    << "Put then content below in a file and use as argument to run"
    << "\n"
    << "# default.properties\n"
    << "exchange=default\n"
    << "routingKey=my.magic.queue\n"
    << "Username=guest\n"
    << "Password=guest\n"
    << "Virtualhost=/\n"
    << "Hostname=localhost\n"
    << "port=5672\n"
    << "requestfifo=/tmp/testfifo\n"
    << "fifoprefix=/tmp/fifo.\n"
    << "clientInstance=Pipe2AMQ\n"
    << "createQueue=my.magic.queue"
    << "noAMQ=false\n"
    << "maxRetry=2\n"
    << "debug=true\n"
    << std::endl;		     
}

int main(int argc,const char *argv[]) {
  if (argc==1) {
    default_config();
    exit(0);
  }
  const std::string configFile{(argc>1)?argv[1]:"test.properties"};
  config::config cfg(configFile);
  sender::wire_sender sender(cfg);
  fifo::request_fifo f(cfg,&sender);
  // struct timespec sleeptime{0,1000*1000};
  struct timespec rem;
  int count;
  while (!f.shutdown()) {
    const auto [received,handled]=f.poll();
    if (received) {
      count=0;
    } else {
      count++;
      if (count>50)
	count=50;
      struct timespec sleeptime{0,count*100*1000};
      nanosleep(&sleeptime,&rem);
    }
  }

  
}
