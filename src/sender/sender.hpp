#ifndef HEADER_sender
#define HEADER_sender

#include <string_view>
#include "../config/config.hpp"

namespace sender {
  class sender {
  protected:
    const config::config &cfg;
  public:
    sender(const config::config &cfg_) : cfg{cfg_} {}
    virtual bool send(std::string_view data)=0;
  };

}

#endif


