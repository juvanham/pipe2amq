#include "wire_sender.hpp"
#include "wire_sender_impl.hpp"

namespace sender {
  
wire_sender::wire_sender(const config::config &cfg_) :
  sender{cfg_}
  {}


wire_sender::~wire_sender()
{
  if (sender_impl)
    sender_impl.reset();
}

  
void wire_sender::sleep_ms(int ms) const {
  struct timespec req {ms /1000, ms%1000};
  struct timespec rem;
  nanosleep(&req,&rem);
}

  
bool wire_sender::send(std::string_view data)
{
  if (cfg.get_noAMQ()) {
    std::cout << "simulate sending [" << data << ']' << std::endl;
    return false;
  }
  if (!sender_impl)
    sender_impl=std::make_unique<wire_sender_impl>(cfg);
  if (!sender_impl) {
    if (cfg.get_debug() ) {
      std::cout << "cannot connect to amq server" << std::endl;
    }
    return false;
  }
  bool sent{false};
  int count{0};
  for (int count{0}; count <cfg.get_maxRetry() && !sent ;count++) {
    sent|=sender_impl->send(data);
    if (!sent)
      sleep_ms(count*300);
  }
  if (!sent)
    sender_impl.reset();
  return sent;
}

}
