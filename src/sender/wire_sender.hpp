#ifndef HEADER_wire_sender
#define HEADER_wire_sender

#include "../config/config.hpp"
#include "./sender.hpp"

namespace sender {
  class wire_sender_impl;
  class wire_sender : public sender {
    std::unique_ptr<wire_sender_impl> sender_impl;
    void sleep_ms(int ms) const;
  public:
    wire_sender(const config::config &cfg_);
    ~wire_sender() ;
    bool send(std::string_view data);
  };

}

#endif


