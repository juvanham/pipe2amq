#include "wire_sender_impl.hpp"

#include <sstream>
#include <unistd.h>
#include <sys/types.h>

namespace sender {
  
  class cstr {
    size_t sz;
    char *const buffer;
  public:
    cstr(std::string_view buf) :
      sz{buf.length()},
      buffer{new char[sz+1]}
    {
      //      memset(buffer, '\0', sz+1);
      buf.copy(buffer,sz);
      *(buffer+sz)='\0';
    }
    
    ~cstr()
    {
      delete [] buffer;
    }
    size_t len() const {
      return sz;
    }
    char* str() {
      return buffer;
    }
    operator char*() {
      return buffer;
    }
    operator size_t() const {
      return sz;
    }
  };
  
  wire_sender_impl::wire_sender_impl(const config::config &cfg_)
    : cfg{cfg_},
      offset{static_cast<unsigned int>(getpid()&0xffff)}
  {
    std::stringstream hostname_stream;
    hostname_stream << cfg.get_Hostname()
		    << ":"
		    << cfg.get_port();
    icl_system_initialise(0, 0);
    std::cout << "hostname " << hostname_stream.str() << std::endl;
    icl_longstr_t *auth_data = amq_client_connection_auth_plain (cstr(cfg.get_Username()),
							cstr(cfg.get_Password())
							);
    connection = amq_client_connection_new (cstr(hostname_stream.str()),
					    cstr(cfg.get_Virtualhost()),
					    auth_data,
					    cstr(cfg.get_clientInstance()),
					    0,
					    30000);
    icl_longstr_destroy (&auth_data);
    if (connection)
      session = amq_client_session_new (connection);

    create_queue(cfg.get_createQueue());
     
  }



  wire_sender_impl::~wire_sender_impl() {
    if (session) {
      amq_client_session_destroy    (&session);
      session=0;
    }
    if (connection) {
      amq_client_connection_destroy (&connection);
      connection=0;
    }
    icl_system_terminate ();
  }

  bool wire_sender_impl::create_queue(std::string_view queuename) {
    if (queuename.empty())
      return false;
    if (!session)
      return false;
    asl_field_list_t *field_list{0};
    icl_longstr_t    *arguments_table{0};
    
    //  Build arguments field as necessary
    field_list = asl_field_list_new (0);
    arguments_table = asl_field_list_flatten (field_list);
    const int rc{amq_client_session_queue_declare (
						   session,                    //  Session reference
						   0,                          //  Access ticket (unused)
						   cstr(queuename),        
						   0,                          //  If 1, don't actually create
						   0,                          //  Durable (unused)
						   0,                          //  If 1, request exclusive queue
						   1,                          //  If 1, auto-delete when unused
						   arguments_table)};           //  Arguments for declaration
    asl_field_list_unlink (&field_list);
    icl_longstr_destroy (&arguments_table);
    return (!rc);
  }

  bool wire_sender_impl::send(std::string_view message) {
    if (!session)
      return false;
    //  Create a content and send it to the "queue" exchange
    auto content = amq_content_basic_new ();
    msg_counter++;
    std::stringstream message_id;
    message_id << offset << msg_counter;
    amq_content_basic_set_body       (content,
				      cstr(message),
				      cstr(message),
				      0);
    amq_content_basic_set_message_id (content, cstr(message_id.str()));
    std::string exchange=cfg.get_exchange();
    if (exchange=="default")
      exchange="";
    const int rc{amq_client_session_basic_publish (
				      session,
				      content,
				      0,
				      cstr(exchange),
				      cstr(cfg.get_routingKey()),
				      true,
				      false)
    };
    amq_content_basic_unlink(&content);				    
    if (rc)
      return false;
    return true;
  }

  
}
