#ifndef HEADER_wire_sender_impl
#define HEADER_wire_sender_impl

#include "../config/config.hpp"

#include <asl.h>
#include <amq_client_connection.h>
#include <amq_client_session.h>

namespace sender {
  class wire_sender_impl {
    const config::config &cfg;
    amq_client_connection_t *connection{0};
    amq_client_session_t *session{0};
    amq_content_basic_t *content{0};
    unsigned long msg_counter{0};
    unsigned long offset{0};
    bool create_queue(std::string_view queuename);
  public:
    wire_sender_impl(const config::config &cfg_);
    ~wire_sender_impl();
    bool send(std::string_view message);
  };
}


#endif
